package com.sda.advanced.Generics.ex3;

import java.util.ArrayList;
import java.util.List;

public class Generic<T> {

    private List<T> myArray;

    public Generic() {
        this.myArray = new ArrayList<>();
    }

    public Generic(List<T> myArray) {  //acest constructor face acelasi lucru ca un set
        this.myArray = myArray;        //asa specificam ca genericul are  aceasta lista
    }

    public void setMyArray(List<T> myArray){
        this.myArray = myArray;
    }

    public List<T> getMyArray(){
        return myArray;
    }

    @Override                                            //faptul ca iteram direct in toString ne ajuta sa nu mai iteram pt fiecare obiect in parte cand il instantiem.Asa, doar vom apela metoda in main
    public String toString() {
        StringBuilder sbMyArray = new StringBuilder();   //stringBuilderul in acest caz ne ajuta sa concatenam un string cu un integer,double..etc

        for (T a : myArray) {
            sbMyArray.append(a);
            sbMyArray.append("  ");
        }

        return sbMyArray.toString();
    }

}