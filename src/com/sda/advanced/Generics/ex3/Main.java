package com.sda.advanced.Generics.ex3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * /**
 *  * Create a simple Generic class, that will give a possibility,
 *  * to store any kind of value within. Add object of type String,
 *  * Integer and Double to array of that Generic type. Print all values of the array within a loop.
 *  */

public class Main{
    public static void main(String[] args) {
        List<Integer> myInteger = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Double> myDouble = new ArrayList<>(Arrays.asList(1.5, 2.5, 3.5));
        List<String> myString = new ArrayList<>(Arrays.asList("day", "month", "year"));

        Generic<Integer> genericInteger = new Generic<>(myInteger);
        Generic<Double> genericDouble = new Generic<>(myDouble);
        Generic<String> genericString = new Generic<>(myString);

        System.out.println(genericInteger);
        System.out.println(genericDouble);
        System.out.println(genericString);
    }
}