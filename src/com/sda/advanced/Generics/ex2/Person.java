package com.sda.advanced.Generics.ex2;

public class Person implements Comparable<Person> {

    private final double height;

    public Person(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public void compareTo(Person otherPerson) {
        if (this.getHeight() == otherPerson.getHeight()) {
            System.out.println("They are the same height ");
        } else {
            if (this.getHeight() < otherPerson.getHeight()) {
                System.out.println("Person is smaller then the otherPerson");
            } else if (this.getHeight() > otherPerson.getHeight()) {
                System.out.println("Person is taller then the otherPerson");
            }else {
                System.out.println("Person is smaller then the otherPerson");
            }
        }
    }

    @Override
    public String toString() {
        return "Person{" +
                "height=" + height +
                '}';
    }
}
