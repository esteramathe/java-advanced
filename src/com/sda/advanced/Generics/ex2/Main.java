package com.sda.advanced.Generics.ex2;

/**
 * Create a Person class that will implement a Comparable interface.
 * Person class should implement compareTo method, that will verify if one person is taller than another.
 */
public class Main {
    public static void main(String[] args) {
        Person pers1=new Person(1.60);
        Person pers2=new Person(1.70);
        Person pers3=new Person(1.80);

        pers1.compareTo(pers2);
        pers2.compareTo(pers3);
        pers3.compareTo(pers1);

    }
}
