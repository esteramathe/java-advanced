package com.sda.advanced.Generics.ex2;

public interface Comparable<T> {

  void compareTo(T o);
}
