package com.sda.advanced.Generics.ex1;

public interface Identifiable {

    String getId();

    String getName();
}
