package com.sda.advanced.Generics.ex1;

import java.util.List;

public interface GenericRepository<T extends Identifiable> {



    void addItem(T item);

    void removeItem(T item)throws ItemNotFoundException;

   List<T> searchItemByName(String name)throws ItemNotFoundException;

   List<T> searchItemsById(String id)throws ItemNotFoundException;

  void removeItemById(String id)throws ItemNotFoundException;

}
