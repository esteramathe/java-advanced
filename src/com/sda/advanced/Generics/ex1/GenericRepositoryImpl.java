package com.sda.advanced.Generics.ex1;

import java.util.ArrayList;
import java.util.List;

public class GenericRepositoryImpl<T extends Identifiable> implements GenericRepository<T>{   //T din stanga sa aibe aceleasi proprietati ca si T din dreapta
    private final List<T> items = new ArrayList<>();

    @Override
    public void addItem(T item){
        items.add(item);
    }

    @Override
    public void removeItem(T item) throws ItemNotFoundException {
        if (items.contains(item)) {
            items.remove(item);
        } else {
            throw new ItemNotFoundException("Item not found" + item);
        }
    }

    @Override
    public List<T> searchItemByName(String name) throws ItemNotFoundException {
        List<T> itemsFound = new ArrayList<>();
        for (T item : items) {  //caut toate maisnile iteme au numele dat ca parametru
            if (name.equals(item.getName())) {
                itemsFound.add(item);
            }
        }
        if (itemsFound.isEmpty()) {
            throw new ItemNotFoundException("items not found for name " + name);
        }
        return itemsFound;
    }

    @Override
    public List<T> searchItemsById(String id) throws ItemNotFoundException {
        return new ArrayList<>();
        //homework
    }

    @Override
    public void removeItemById(String id) throws ItemNotFoundException {
        List<T> itemsFound = new ArrayList<>();
        for (T item : items) {
            if (id.equals(item.getId())) {
                itemsFound.add(item);
            }
        }
        if (itemsFound.isEmpty()) {
            throw new ItemNotFoundException("items not found for name " + id);
        }
        items.removeAll(itemsFound);
    }

}
