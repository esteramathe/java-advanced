package com.sda.advanced.Generics.ex1;

public class Main {
    public static void main(String[] args) throws ItemNotFoundException {


        GenericRepository<Car> carRepository = new GenericRepositoryImpl<>();


        Car car1 = new Car("1", "BMW", 2020);
        Car car2 = new Car("2", "OPEL", 2021);
        Car car3 = new Car("3", "SKODA", 2022);

        carRepository.addItem(car1);
        carRepository.addItem(car2);


        try {
            carRepository.removeItem(car2);
        } catch (ItemNotFoundException e) {

            System.out.println(e.getMessage());
        }
        try {
            carRepository.removeItem(car3);
        } catch (ItemNotFoundException e) {
            System.out.println(e.getMessage());
        }
        carRepository.addItem(car2);

        carRepository.addItem(car3);

        try {
            System.out.println((carRepository.searchItemByName("BMW")));
        } catch (ItemNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            carRepository.removeItemById("1");
        } catch (ItemNotFoundException e) {
            System.out.println(e.getMessage());
        }

        try {
            carRepository.removeItemById("1");
        } catch (ItemNotFoundException e) {
            System.out.println(e.getMessage());
        }
        GenericRepository<Person> personGenericRepository = new GenericRepositoryImpl<>();   //in stanga este interfata si in dreapta implementarea
        Person person1 = new Person("1", "Ana");
        Person person2 = new Person("2", "Ion");
        personGenericRepository.addItem(person1);
        personGenericRepository.addItem(person2);
        System.out.println(personGenericRepository.searchItemsById("1"));

    }
}

