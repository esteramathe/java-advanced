package com.sda.advanced.Generics.ex1;

public class ItemNotFoundException extends Exception{

    public ItemNotFoundException(String message) {
        super(message);
    }
}
