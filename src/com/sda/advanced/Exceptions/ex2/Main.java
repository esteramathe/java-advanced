package com.sda.advanced.Exceptions.ex2;

public class Main {
    public static void main(String[] args) {
        /*try
        {
            Files.delete(Paths.get("somePath"));
        }
        catch (IOException e)
        {
            // Do something first with the exception
            System.out.println(e.getMessage());
            throw e;
        }*/


        try {
            checkTime();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private static void checkTime() throws Exception {
        if (System.currentTimeMillis() % 2 == 0) {
            throw new Exception("Time is not fine!");
        }

        System.out.println("Time is ok");
    }
}





