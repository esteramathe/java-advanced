package com.sda.advanced.Exceptions.ex1;

import java.util.Scanner;

/**
 * Write an application that will read the input and print back value that user provided, use try-catch statements to parse the input, e.g.
 * 	I/O:
 * 	Input: 10
 * 	Output: int -> 10
 * 	Input: 10.0
 * 	Output: double -> 10.0
 * Input: „Hello!”
 * 	Output: „Hey! That’s not a value! Try once more.”
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input: ");
        String stringInput = scanner.nextLine();
        try {
            int intInput = Integer.parseInt(stringInput);
            System.out.println("int -> " + intInput); //aici verificam ca este int prin afisare
        } catch (NumberFormatException e) {
            try {    //am pus try-ul al2lea in catch ca sa mi-l execute daca nu e int.Daca nu ar fi un int ceea ce s-a introdus de la tastatura atunci nu s-ar mai fi executat restul
                double doubleInt = Double.parseDouble(stringInput);
                System.out.println("double-> " + doubleInt); //aici verificam ca este double prin afisare
            } catch (NumberFormatException ex) {
                System.out.println("Hey!That's not a value!Try once more.");
            } finally {
                System.out.println("This line works every time -> double");
            }

        } finally {    //acest finally se executa doar daca se executa tryul de sus! daca bag un int de la tastatura nu mai intra in primul catch.dar daca bag un double atunci intra in primul catch
            System.out.println("This line wotks every time -> int");
        }
        // !!!!  finally se executa doar daca se executa tryul, altfel nu
    }
}
