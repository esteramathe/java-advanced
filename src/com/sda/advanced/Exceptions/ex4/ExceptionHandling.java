package com.sda.advanced.Exceptions.ex4;

public class ExceptionHandling {
    public static void main(String[] args) {
        try {
            int myInt = Integer.parseInt("pants");
            System.out.println("After parsing"); //this line will not be executed if we catch an exeption from line above
        } catch (NumberFormatException | NullPointerException e) {
            System.out.println("Hey, you can not make an int out of that!");
        } finally {     //finally block always will be executed NO METTER WHAT!
            System.out.println("In the finally block");
        }
        System.out.println("End");

        System.out.println(printANumber());
    }

    private static int printANumber() {
        try {
            return 3;
        } catch (Exception e) {
            return 2;
        } finally {
            return 5;            //the finally block override the try block!always is executed!
        }
    }
}

