package com.sda.advanced.Exceptions.ex3;

import java.util.ArrayList;
import java.util.List;

public class CarRepositoryImpl implements CarRepository {
    private List<Car> cars = new ArrayList<>();
    //ramane interfata daca implementez toate metodele
    //daca nu implementez toate metodele atunci fac clasa abstracta

    @Override
    public void addCar(Car car) throws CarNotFoundException {
        cars.add(car);
    }

    @Override
    public void removeCar(Car car) throws CarNotFoundException {
        if (cars.contains(car)) {                //am in lista cars, car pe care l-am primit ca parametru?
            cars.remove(car);                    //daca il gaseste il sterge
        } else {
            throw new CarNotFoundException("Car not found" + car);
        }
    }

    @Override
    public Car[] searchCarsByName(String name) throws CarNotFoundException {
        List<Car> carsFound = new ArrayList<>();
        for (Car car : cars) {  //caut toate maisnile care au numele dat ca parametru si le pun intr-o lista
            if (name.equals(car.getName())) {
                carsFound.add(car);
            }
        }
        if (carsFound.isEmpty()) {
            throw new CarNotFoundException("Cars not found for name " + name);
        }
        return carsFound.toArray(new Car[carsFound.size()]);
    }

    @Override
    public Car[] searchCarsById(String id) throws CarNotFoundException {
        List<Car> carsWithIdFound = new ArrayList<>();
        for (Car car : cars) { //caut toate masinile care au id ca parametru si le pun intr-o lista
            if (id.equals(car.getId())) {
                carsWithIdFound.add(car);
            }
        }
        if (carsWithIdFound.isEmpty()) {
            throw new CarNotFoundException("Cars not found for id " + id);
        }
        return carsWithIdFound.toArray(new Car[carsWithIdFound.size()]);
        //homework
    }

    @Override
    public void removeCarById(String id) throws CarNotFoundException {
        List<Car> carsFound = new ArrayList<>();
        for (Car car : cars) {  //caut toate maisnile care au idul dat ca parametru si le pun intr-o lista si daca nu gaseste ceva aruncam exceptia, daca gaseste ceva stergem cu removeall
            if (id.equals(car.getId())) {
                carsFound.add(car);
            }
        }
        if (carsFound.isEmpty()) {
            throw new CarNotFoundException("Cars not found for name " + id);
        }
        cars.removeAll(carsFound);
    }
}
