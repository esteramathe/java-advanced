package com.sda.advanced.Exceptions.ex3;

import java.util.ArrayList;
import java.util.List;

public interface CarRepository {



    void addCar(Car car)throws CarNotFoundException;  //daca adaug nu returnez nimic

    void removeCar(Car car)throws CarNotFoundException; //daca sterg nu returnez nimic

    Car[] searchCarsByName(String name)throws CarNotFoundException; //daca ,caut returnez ceva.aici am o metoda care cauta toate obiectele de tip car -> array de car

    Car[] searchCarsById(String id)throws CarNotFoundException;

    void removeCarById(String id)throws CarNotFoundException;

}
