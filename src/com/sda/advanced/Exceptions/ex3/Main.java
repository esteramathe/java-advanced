package com.sda.advanced.Exceptions.ex3;

import java.util.Arrays;

/**
 * Create the CarRepository class, which will be responsible for:
 * - adding Car objects
 * - removing Car objects
 * - searching for objects of the Car type with the indicated name
 * - searching for objects of the Car type with the indicated id
 * - removing objects of the Car type based on the provided id
 * <p>
 * The Car class should include the following:
 * - id
 * - name
 * - year of production
 * <p>
 * In case of lack of searched results an exception should be thrown. This
 * exception should accept the String parameter object with information about
 * which elements could not be found.
 */
public class Main {
    public static void main(String[] args) throws CarNotFoundException {

        //cand am interfata declar cu interfata si initializez cu clasa

        CarRepository carRepository = new CarRepositoryImpl();
        Car car1 = new Car("1", "BMW", 2020);
        Car car2 = new Car("2", "OPEL", 2021);
        Car car3 = new Car("3", "SKODA", 2022);

        carRepository.addCar(car1);
        carRepository.addCar(car2);


        try {
            carRepository.removeCar(car2);
        } catch (CarNotFoundException e) {

            System.out.println(e.getMessage());
        }
        try {
            carRepository.removeCar(car3);
        } catch (CarNotFoundException e) {
            System.out.println(e.getMessage());
        }
        carRepository.addCar(car2);

        carRepository.addCar(car3);

        try{
            System.out.println(Arrays.toString(carRepository.searchCarsByName("BMW")));
        }catch (CarNotFoundException e){
            System.out.println(e.getMessage());
        }
        try {
            carRepository.removeCarById("1");
        } catch (CarNotFoundException e) {
            System.out.println(e.getMessage());
        }

        try {
            carRepository.removeCarById("1");
        } catch (CarNotFoundException e) {
            System.out.println(e.getMessage());
        }

       try{
           carRepository.searchCarsById("1");
       }catch (CarNotFoundException e){
           System.out.println(e.getMessage());
       }
    }
}

