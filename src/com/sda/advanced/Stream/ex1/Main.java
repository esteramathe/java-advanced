package com.sda.advanced.Stream.ex1;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    private static final List<Product> productList = Arrays.asList(
            new Product("Kahlua", "$", 4.57, "Fish"),
            new Product("Cheese - Comte", "$", 6.05, "Meat"),
            new Product("Oil - Margarine", "$", 7.08, "Meat"),
            new Product("Wine - Stoneliegh Sauvignon", "$", 3.79, "Meat"),
            new Product("Anchovy In Oil", "$", 5.51, "Meat"),
            new Product("Tomatoes Tear Drop Yellow", "$", 8.08, "Meat"),
            new Product("Wonton Wrappers", "$", 6.29, "Meat"),
            new Product("Stock - Veal, Brown", "$", 9.85, "Meat"),
            new Product("Beef - Flank Steak", "$", 6.29, "Meat"),
            new Product("Bandage - Fexible 1x3", "$", 8.99, "Meat"),
            new Product("Toothpick Frilled", "$", 4.25, "Fish"),
            new Product("Dry Ice", "$", 2.89, "Fish"),
            new Product("Veal - Inside", "$", 4.30, "Fish"),
            new Product("Bacardi Raspberry", "$", 2.22, "Fish"),
            new Product("Glucose", "$", 8.13, "Fish"),
            new Product("Tomato Puree", "$", 5.02, "Fish"),
            new Product("Sauce - Soya, Dark", "$", 2.53, "Fish"),
            new Product("Fish - Soup Base, Bouillon", "$", 5.10, "Fish"),
            new Product("Cactus Pads", "$", 2.50, "Fish"),
            new Product("Pasta - Fettuccine, Dry", "$", 2.51, "Fish")
    );

    public static void main(String[] args) {
        productList.stream()
                .filter(product -> "Fish".equals(product.getType()))
                .filter(product -> product.getPrice() > 5)
                .forEach(product -> System.out.println(product));
        productList.stream()
                .map(product -> product.getPrice()) //aici convertim din obiect in pretul obiectului
                .sorted()
                .forEach(price -> System.out.println(price));  //aici avem price pentru ca sus am convertit cu mapul din obiect in price si acum luvram cu priceul nu mai cu obiectul

        List<Product> products = productList.stream()
                .filter(product -> product.getPrice() > 2)
                .filter(product -> "$".equals(product.getCurrency()))
                .sorted((o1, o2) -> Double.compare(o2.getPrice(), o1.getPrice()))
                .collect(Collectors.toList());
        products.forEach(product -> System.out.println(product)); //daca nu e convertit la method references adica cu 2 puncte putem usor sa modificam ce va iesi pe sout.daca am avea cu 2 puncte nu mai putem modifica

        Map<Double,List<Product>> productsByPrice = productList.stream()                //pt ca putem avea 2 produse cu acelasi pret
                .collect(Collectors.groupingBy(product -> product.getPrice()));
        productsByPrice.forEach((price, products2) -> System.out.println(price + "  "));
    }
}
