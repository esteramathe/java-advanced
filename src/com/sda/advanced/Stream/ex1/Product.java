package com.sda.advanced.Stream.ex1;

public class Product
{
    private String name;
    private String currency;
    private double price;
    private String type;

    public Product(String name, String currency, double price, String type)
    {
        this.name = name;
        this.currency = currency;
        this.price = price;
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "Product{" +
                "name='" + name + '\'' +
                ", currency='" + currency + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
                '}';
    }
}
