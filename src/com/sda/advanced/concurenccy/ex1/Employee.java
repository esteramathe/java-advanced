package com.sda.advanced.concurenccy.ex1;

import java.time.Instant;

public class Employee implements Runnable{

    private String name;

    public Employee(String name){
        this.name=name;
    }
    @Override
    public void run() {
        System.out.println(name+"I came to work at "+Instant.now());
        while(true){                                                       //adica intotdeauna executa threadul sleep
            try {
                Thread.sleep(1000);
                System.out.println("I'm still working!");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }


}
