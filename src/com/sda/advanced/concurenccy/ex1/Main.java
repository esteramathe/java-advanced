package com.sda.advanced.concurenccy.ex1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Concurrency Exercise 4
 * *You are the manager. You have 5 employees.
 * Simulate the situation in which each of them comes at a different time to work.
 * - Every employee, after getting to work, displays the information "<name: I came to work at <time HH:MM>."
 * - Every 10 seconds, the employee displays „name: I'm still working!"
 * c) Every 30 seconds, we release one of the employees to home (remember about stopping the thread!)
 * and remove the employee from the „active employees list"
 * - When you release your employee to home, print „, it's time to go home!"
 * - *When you release a given employee, all of the others speed up. From that moment, display the information about work 2 seconds faster. f)
 * The manager decides in which order release employees (e.g. through an earlier defined list)
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        //with Runnable Interface example
     /*   List<Thread> employeeList=new ArrayList<>();
        for (int i=0; i<=5;i++){
            employeeList.add(new Thread(new Employee("employee+ "+i)));  //aici folosind interfata Thread nu pot instantia direct pe clasa employee.trebuie intai new THhread
        }
        for(Thread thread:employeeList){
            Thread.sleep(new Random().nextInt(5000));
            thread.start();
        }
*/
        List<EmployeeThread> employeeList = new ArrayList<>();
        for (int i = 0; i <= 5; i++) {
            employeeList.add(new EmployeeThread("employee+ " + i));
        }
        for (Thread thread : employeeList) {
            Thread.sleep(new Random().nextInt(5000));
            thread.start();
        }
        while (!employeeList.isEmpty()) {
            int index = 0;
            if (employeeList.size() > 1) {                                             //trebuie sa verificam daca lista are mai mult de un element ca sa putem face random
                index = new Random().nextInt(0, employeeList.size() - 1); //indexul asta reprezinta angajatul pe care il dau afara
            }
            Thread.sleep(1500);                                                  //aici o aleg o pozitie din lista de unde sa scot angajatul s ail trimit acasa
            employeeList.get(index).goHome();
            employeeList.remove(index);
        }
        for (EmployeeThread thread : employeeList) {
            Thread.sleep((1500));
            thread.goHome();
        }

    }

}
