package com.sda.advanced.Enum.ex1;

import java.sql.SQLOutput;

/**
 * Create enum Planet.
 * a) Override toString method. It should print relative size of a planet and it’s name.
 * E.g. „Huge Jupiter”, „Small Pluto”.
 * b)  Create distanceFromEarth method.
 * c) Verify both methods for multiple planets.
 */
public class Main {
    public static void main(String[] args) {
        for (Planet value : Planet.values()) {
            System.out.print(value);
            System.out.println(" distance from Earth " + value.getDistanceFromEarth());
        }

    }
}
