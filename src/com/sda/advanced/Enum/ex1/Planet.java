package com.sda.advanced.Enum.ex1;

public enum Planet {
    JUPITER("Huge", "Jupiter", 12347756),
    PLUTO("Small", "Pluto", 12345674),
    MARS("Small", "Mars", 12345678),
    TERRA("Normal", "Terra", 0),
    URANUS("Huge", "Uranus", 1244566797);

    private final String relativeSize;
    private final String prettyName;
    private final long distanceFromEarth;

    Planet(String relativeSize, String prettyName, long distanceFromEarth) {
        this.relativeSize = relativeSize;
        this.prettyName = prettyName;
        this.distanceFromEarth = distanceFromEarth;
    }

    public long getDistanceFromEarth() {
        return distanceFromEarth;
    }

    @Override
    public String toString() {
        return relativeSize.concat(" ").concat(prettyName);
    }
}
