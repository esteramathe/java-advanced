package com.sda.advanced.Enum.ex2;

public enum Weekday {
    MONDAY("weekday"),
    TUESDAY("weekday"),
    WEDNESDAY("weekday"),
    THURSDAY("weekday"),
    FRIDAY("weekday"),
    SATURDAY("holiday"),
    SUNDAY("holiday");

    public final String dayType;

    Weekday(String dayType) {
        this.dayType = dayType;
    }

    public boolean isWeekday() {
        return this.dayType.equals("weekday");
    }

    public boolean isHoliday() {
        if (this.dayType.equals("holiday")) {
            return !this.isWeekday();
        } else {
            return false;
        }
    }

    public void myDay() {
        switch (this) {
            case MONDAY -> System.out.println("Monday-is the first day of the week!");
            case TUESDAY -> System.out.println("Tuesday-is the second day of the week!");
            case WEDNESDAY -> System.out.println("Wednesday-is the third day of the week!");
            case THURSDAY -> System.out.println("Thursday-is the forth day of the week!");
            case FRIDAY -> System.out.println("Friday-is the fifth day of the week-almost holiday!");
            case SATURDAY -> System.out.println("Saturday-is the first day of holiday!");
            case SUNDAY -> System.out.println("Sunday-is the last day of holiday!");
            default -> System.out.println("Have a nice day!");
        }
    }
}





