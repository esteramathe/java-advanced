package com.sda.advanced.InputOut.ex1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create a file with a „lorem ipsum" paragraph within – it can be done by copy-pasting existing paragraph or generating it dynamically using Java library.
 * - Read that file.
 * - Count words. //cate cuvinte am din fiecare (dubluri)
 * - *Count special signs (like comma, dot, spaces).
 * - *Select one word and print it's number of occurrences.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("C:\\Users\\40744\\IdeaProjects\\JavaAdvanced\\src\\com\\sda\\advanced\\InputOut\\ex1\\loremipsum.txt");
        List<String> lines = Files.readAllLines(path);                      //punem in semnatura exceptia pt ca fisierul poate sa existe sau nu //clasa Files este clasa utilitara cu metode statice, nu o initializam
        Map<String, Integer> noOfWords = new HashMap<>();                   //iterare pe fiecare linie
        for (String line : lines) {
            for (String word : line.split(" ")) {                      //aici sparg linia in cuvinte si iterez peste cuvinte//pentru fiecare cuvant din linie fac ceva
                noOfWords.putIfAbsent(word, 0);                                                   //aici daca e prima data cand intalnesc un cuvant il pun.asta doar daca e prima data.la a doua tura se va ignora
                int occurences = noOfWords.get(word);                        //scot valoarea din map pt cuvantul meu care e cheia
                noOfWords.put(word, occurences + 1);                           // +1 e in loc de ++
            }
        }
        System.out.println(noOfWords);
        //cate aparitii am pt cuvantul ipsum?:
        System.out.println(noOfWords.get("ipsum"));
    }
}
        //de exemplu pentru semne de punctuatie ar trebui sa spargem si cuvintele in litere si ti map in functie de litere
//nr de aparitii in functie de litere