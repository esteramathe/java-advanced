package com.sda.advanced.recap.ex1.repository;

import com.sda.advanced.recap.ex1.Sellable;

import java.util.*;
import java.util.stream.Collectors;

public class SellableRepositoryimpl<T extends Sellable> implements SellableRepository<T> {

    private Map<String, T> sellableByName = new HashMap<>();

    @Override
    public void add(T sellable) {
        sellableByName.put(sellable.getName(), sellable);
    }

    @Override
    public Optional<T> search(String name) {    //ceea ce scot din Map poate sa fie null deci pun ofNulablle
        return Optional.ofNullable(sellableByName.get(name));
    }

    @Override
    public void remove(String name) {
        sellableByName.remove(name);
    }

    @Override
    public List<T> findAllOrderedByPrice() {
        return sellableByName.values().stream()
                .sorted((o1, o2) -> Double.compare(o1.getPrice(), o2.getPrice()))
                .collect(Collectors.toList());
    }
}
