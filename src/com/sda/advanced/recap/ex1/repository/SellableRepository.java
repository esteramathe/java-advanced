package com.sda.advanced.recap.ex1.repository;

import com.sda.advanced.recap.ex1.Sellable;

import java.util.List;
import java.util.Optional;

public interface SellableRepository <T extends Sellable>{ //e interfata de baza vreau sa pot lucra cu orice obiect de tip  Sellable
    void add(T sellable);
    Optional<T> search(String name);  //optional de T ca poate exista poate nu
    void remove (String name);
    List<T> findAllOrderedByPrice();


}
