package com.sda.advanced.recap.ex1.repository;

import com.sda.advanced.recap.ex1.Sellable;
import com.sda.advanced.recap.ex1.model.GroceryProduct;

public interface GroceryProductRepository extends SellableRepository<GroceryProduct> {
}
