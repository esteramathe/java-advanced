package com.sda.advanced.recap.ex1;

public interface Sellable extends Comparable<Sellable> {     //extinde Comparable de tip Sellable

    double getPrice();              //public si abstract by default

    default String getCurrency() {
        return "EUR";               //vreau ca metoda mea sa fie euro default; //asta nu e obligatoriu sa o interpretez dar o pot suprascrie

    }
    String getName();

    String getDescription();

    String getProductCategory();  //o putem implementa ca stim categoria ca e food

}
