package com.sda.advanced.recap.ex1;

import com.sda.advanced.recap.ex1.model.GroceryProduct;
import com.sda.advanced.recap.ex1.repository.GroceryProductRepository;
import com.sda.advanced.recap.ex1.repository.GroceryProductRepositoryimpl;

import java.util.Date;

/**
 *
 */
public class Main {
    public static void main(String[] args) {

        // interfata = implementare -> pt flexibilitate
        GroceryProductRepository groceryProductRepository = new GroceryProductRepositoryimpl();

        groceryProductRepository.add(
                new GroceryProduct(
                        new Date(),
                        true,
                        "Romania",
                        5.20,
                        "Sugar",
                        "SweetSugar"
                )
        );

        groceryProductRepository.findAllOrderedByPrice().stream()
                .forEach(groceryProduct -> System.out.println(groceryProduct)); //pt fiecare produs sa il scoata pe sout
    }
}
