package com.sda.advanced.recap.ex1.model;

public enum CustomerState {
    NEW(false, "New"),
    ACTIVE(true,"Active"),
    CLOSE(false,"Close"),
    BANNED(false,"Banned");

    private boolean shoppingAllowed;                           //sa nu pot lasa pe oricine sa cumpere
    private String displayName;

    CustomerState(boolean shoppingAllowed, String displayName) {   //constructorul e private
        this.shoppingAllowed = shoppingAllowed;
        this.displayName = displayName;
    }

    public boolean isShoppingAllowed() {
        return shoppingAllowed;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
