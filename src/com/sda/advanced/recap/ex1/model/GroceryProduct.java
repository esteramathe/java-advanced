package com.sda.advanced.recap.ex1.model;

import java.util.Date;

public class GroceryProduct extends FoodProduct {

    private double price;
    private String name;
    private String description;

    public GroceryProduct(Date expirationDate,
                          boolean isBio,
                          String countryOfOrigin,
                          double price,
                          String name,
                          String description) {
        super(expirationDate, isBio, countryOfOrigin);

        if (price > 0) {
            this.price = price;                //validam pretul
        } else {
            throw new IllegalArgumentException("Invalid price");
        }
        this.name = name;
        if (name != null && !name.isBlank()) {
            throw new IllegalArgumentException("Invalid name!"); //un-checked exception
        }
        this.description = description;
        if (description != null && !description.isBlank()) {
            throw new IllegalArgumentException(("Invalid description!"));
        }
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "GroceryProduct{" +
                "price=" + price +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
