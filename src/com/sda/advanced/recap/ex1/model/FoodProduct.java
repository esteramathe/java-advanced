package com.sda.advanced.recap.ex1.model;

import com.sda.advanced.recap.ex1.Sellable;

import java.util.Date;

public abstract class FoodProduct implements Sellable {
    private Date expirationDate;
    private boolean isBio;
    private String countryOfOrigin;

    public FoodProduct(Date expirationDate, boolean isBio, String countryOfOrigin) {
        this.expirationDate = expirationDate;
        this.isBio = isBio;
        this.countryOfOrigin = countryOfOrigin;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isBio() {
        return isBio;
    }

    public void setBio(boolean bio) {
        isBio = bio;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    @Override
    public String getProductCategory() {
        return "Food";
    }

    @Override
    public int compareTo(Sellable o) {
        return Double.compare(getPrice(), o.getPrice()); //compar priceul meu cu priceul din constructor
    }

    @Override
    public String toString() {
        return "FoodProduct{" +
                "expirationDate=" + expirationDate +
                ", isBio=" + isBio +
                ", countryOfOrigin='" + countryOfOrigin + '\'' +
                '}';
    }
}
