package com.sda.advanced.recap.ex1.model;

import java.util.Date;
import java.util.Objects;

public class Account {
    private String id;
    private String billingAdress;
    private boolean isClosed;
    private Date creationDate;
    private Date closedDate;

    public Account(String id, String billingAdress, boolean isClosed, Date creationDate, Date closedDate) {
        this.id = id;
        this.billingAdress = billingAdress;
        this.isClosed = isClosed;
        this.creationDate = creationDate;
        this.closedDate = closedDate;
    }

    public Account(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillingAdress() {
        return billingAdress;
    }

    public void setBillingAdress(String billingAdress) {
        this.billingAdress = billingAdress;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Date closedDate) {
        this.closedDate = closedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return isClosed == account.isClosed && Objects.equals(id, account.id) && Objects.equals(billingAdress, account.billingAdress) && Objects.equals(creationDate, account.creationDate) && Objects.equals(closedDate, account.closedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, billingAdress, isClosed, creationDate, closedDate);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", billingAdress='" + billingAdress + '\'' +
                ", isClosed=" + isClosed +
                ", creationDate=" + creationDate +
                ", closedDate=" + closedDate +
                '}';
    }
}
