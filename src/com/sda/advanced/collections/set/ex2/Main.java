package com.sda.advanced.collections.set.ex2;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Collections SET Exercise 2
 * - Write a Java program to iterate through all elements in a hash set.
 * - Write a Java program to empty a hash set.
 * - Write a Java program to test a hash set is empty or not.
 * - Write a Java program to clone a hash set to another hash set.
 * - Write a Java program to convert a hash set to an array.
 * - Write a Java program to convert a hash set to a tree set.
 * - Write a Java program to compare two sets and retain elements which are same on both sets.
 */
public class Main {
    public static void main(String[] args) {
        HashSet<String> seasons = new HashSet<>();
        seasons.add("Winter");
        seasons.add("Fall");
        seasons.add("Spring");
        seasons.add("Summer");
        System.out.println(seasons);


       Iterator<String> iterator=seasons.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        seasons.clear();
        System.out.println("Is the set empty: "+ true);

        seasons.add("Winter");
        seasons.add("Summer");
        System.out.println(seasons);

        Set<String> newSet=new HashSet<>();
        newSet.add("a");
        newSet.add("b");
        System.out.println(newSet);

        Set<String> clonedSet=new HashSet<>(newSet);  //se creaza aceleasi obiect dar cu alta adresa de memorie
        System.out.println("Cloned set: "+clonedSet);













    }
}
