package com.sda.advanced.collections.list.arraylist.ex2;

import java.util.*;
/**
 * Write a Java program to create a new array list, add some colors (string) and print out the collection.
 * - Write a Java program to iterate through all elements in a array list.
 * - Write a Java program to insert an element into the array list at the first position.
 * - Write a Java program to retrieve an element (at a specified index) from a given array list.
 * - Write a Java program to update specific array element by given element.
 * - Write a Java program to remove the third element from a array list.
 * - Write a Java program to search an element in a array list.
 * - Write a Java program to sort a given array list.
 * - Write a Java program to copy one array list into another.
 * - Write a Java program to shuffle elements in a array list.
 * - Write a Java program to reverse elements in a array list.
 * - Write a Java program to extract a portion of a array list.
 * - Write a Java program of swap two elements in an array list.
 * - Write a Java program to join two array lists.
 * - Write a Java program to clone an array list to another array list.
 * - Write a Java program to empty an array list.
 * - Write a Java program to test an array list is empty or not.
 */

public class Main {
    public static void main(String[] args) {
        List<String> colors = new ArrayList<>();
        colors.add("red");
        colors.add("blue");
        colors.add("green");
        colors.add("pink");
        System.out.println(colors);

        List<String> letters = new ArrayList<>();
        letters.add("a");
        letters.add("b");
        letters.add("c");
        System.out.println(letters);

        for (String myColor : colors) {
            System.out.println(myColor);
        }

        colors.add(0, "purple");
        System.out.println(colors);

        String element = colors.get(0);
        System.out.println(element);

        colors.remove(2);
        System.out.println(colors);

        colors.set(0, "yellow");
        System.out.println(colors);

        colors.remove(3);
        System.out.println(colors);

        if (colors.contains("red")) {
            System.out.println("Red is on the list");
        } else {
            System.out.println("Red is not on the list");
        }

        Collections.sort(colors);
        System.out.println("List after sort:\n" + colors);

        Collections.copy(colors, letters);
        System.out.println("List after copy: ");
        System.out.println("List 1: " + colors);
        System.out.println("List 2: " + letters);

        Collections.shuffle(colors);
        System.out.println("After shuffling:\n" + colors);

        Collections.reverse(colors);
        System.out.println("After revers:\n" + colors);

        List<String> subList = colors.subList(0, 2);
        System.out.println("Sub list:\n" + subList);

        Collections.swap(colors, 0, 1);
        System.out.println("After swap:");
        for (String theSwap : colors) {
            System.out.println(theSwap);
        }

        List<String> newList = new ArrayList<>();
        newList.addAll(colors);
        newList.addAll(letters);
        System.out.println(newList);

        List<String> clonedList=new ArrayList<>(newList);
        System.out.println("Cloned list:\n"+clonedList);

        colors.clear();
        System.out.println("After remove all: " + colors);

        System.out.println("Is the list empty: "+colors.isEmpty());

    }
}
