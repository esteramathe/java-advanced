package com.sda.advanced.collections.list.arraylist.ex1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> shoppingList = new ArrayList<>();      //initializez o lista de stringuri cu un array list
        shoppingList.add("Apple");
        shoppingList.add("Cheese");
        shoppingList.add("Milk");
        shoppingList.add(0, "Honey");

        for (String product : shoppingList) {      //generam cu shopping.for
            System.out.println(product);
        }
        shoppingList.sort(new Comparator<String>() {   //sortam lista in functie de ordine, crescatoare
            @Override                                    //sau descrescatoare schimband in return ordinea
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });


        shoppingList.remove("Milk");  //poate sterge dupa nume sau pe index
        shoppingList.remove(0);

        if(shoppingList.contains("Apple")){
            System.out.println("Apple is still on the list!");
        }
        shoppingList.clear();
    }

}
