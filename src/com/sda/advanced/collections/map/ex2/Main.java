package com.sda.advanced.collections.map.ex2;

import java.util.*;

/**
 * ##### Collections MAP Exercise 2
 * Write a program which creates a concordance of characters occurring in a string (i.e., which characters occur where in a string).
 * Read the string from the command line.
 * Ex: {d=[9], o=[4, 6], r=[7], W=[5], H=[0], l=[2, 3, 8], e=[1]}
 */
public class Main {
    public static void main(String[] args) {
        Map<Character, List<Integer>> indexesByCharacterMap = new HashMap<>();

        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();

        // getCharOccurencesV1(indexesByCharacterMap, inputString);

        // getCharOccurencesV2(indexesByCharacterMap, inputString);

        for (int i = 0; i < inputString.length(); i++) {
            char currentCharacter = inputString.charAt(i);
            if (!indexesByCharacterMap.containsKey(currentCharacter)) {             //vreau sa verifica daca NU am caracterul map
                indexesByCharacterMap.put(currentCharacter, new ArrayList<>());     //aici dau key mele o lista goala apoi adaug in lista asta//(daca nu am caracterul atunci fac o lista noua in care il pun)
            } else {
                List<Integer> indexes = indexesByCharacterMap.get(currentCharacter);    //aici lista de indexi primeste valoarea key in cazul in care este prima data
                indexes.add(i);                                                        //daca nu as adauga lista goala am primi NullPointerException pt ca Mapul scoate null daca nu exista nimic, deci era null
                indexesByCharacterMap.put(currentCharacter, indexes);
                                                                                       //nu pun de fiecare data lista goala pt ca dupaia se va rescrie de fiecare data.
            }
            System.out.println(indexesByCharacterMap);
        }                                                                              //daca scot sout in afara forului nu se mai vede piramida.in for se vede cum se construieste, stil piramida

    }
    private static void getCharOccurencesV2(Map<Character, List<Integer>> indexesByCharacterMap, String inputString) {
        for (int i = 0; i < inputString.length(); i++) {
            char currentCharacter = inputString.charAt(i);
            indexesByCharacterMap.putIfAbsent(currentCharacter, new ArrayList<>());
            List<Integer> indexes = indexesByCharacterMap.get(currentCharacter);
            indexes.add(i);
            System.out.println(indexesByCharacterMap);
        }
    }

    private static void getCharOccurencesV1(Map<Character, List<Integer>> indexesByCharacterMap, String inputString) {
        for (int i = 0; i < inputString.length(); i++) {
            char currentCharacter = inputString.charAt(i);
            if (!indexesByCharacterMap.containsKey(currentCharacter)) {
                indexesByCharacterMap.put(currentCharacter, new ArrayList<>());
            }
            List<Integer> indexes = indexesByCharacterMap.get(currentCharacter);
            indexes.add(i);
            System.out.println(indexesByCharacterMap);
        }
    }
}


