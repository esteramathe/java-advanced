package com.sda.advanced.collections.map.ex1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ##### Collections MAP Exercise 1
 * <p>
 * Create a map and display its result (data should be provided by the user - console):
 * - Names and surnames
 * - Names and ages. //homework
 * - Names and lists of friends (other names).
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("-----------------------------Ex1---------------------------------");
        Map<String, String> surnameByNameMap = new HashMap<>(); //intotdeauna am hashmap la implementare, nu altceva
        surnameByNameMap.put("Descultescu", "Alex");
        surnameByNameMap.put("Moise", "Andrei");
        surnameByNameMap.put("Descultescu", "Bogdan");
        System.out.println(surnameByNameMap);

        //pe Map evit iterarea.Nu ma duc pe for
        if (surnameByNameMap.containsKey("Descultescu")) {
            System.out.println("Entry found");
        }

        System.out.println(surnameByNameMap.get("Moise"));
        System.out.println(surnameByNameMap.get("Ion"));  //sa vad daca nu gasesc ce am cautat (da null)

        System.out.println("-----------------------------Ex3---------------------------------");

        Map<String, List<String>> friendsByNameMap = new HashMap<>();//ca valoare am o lista de obiecte de tip string
        friendsByNameMap.put("Badiu", List.of("Alex", "Bogdan"));
        friendsByNameMap.put("Moise", List.of("Ion"));
        friendsByNameMap.put("Moise", List.of("Ion", "MIhai"));
        System.out.println(friendsByNameMap);
        System.out.println(friendsByNameMap.get("Moise"));
        System.out.println(friendsByNameMap.get("Ion"));

    }
}
