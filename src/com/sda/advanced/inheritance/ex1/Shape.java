package com.sda.advanced.inheritance.ex1;

public class Shape {
    private double area;
    private double perimeter;
    private String color;

    public Shape(double area, double perimeter, String color) {
        this.area = area;
        this.perimeter = perimeter;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Shape{" +
                "area=" + area +
                ", perimeter=" + perimeter +
                ", color='" + color + '\'' +
                '}';
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
