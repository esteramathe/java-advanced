package com.sda.advanced.inheritance.ex1;

public class Circle extends Shape{
    private double radius;

    public Circle(String color, double radius) {
        super(Math.PI*radius*radius,Math.PI*2*radius, color);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return super.toString()+"Circle{" +
                "radius=" + radius +
                '}';
    }
}
