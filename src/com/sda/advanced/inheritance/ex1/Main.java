package com.sda.advanced.inheritance.ex1;

/**
 * Create a Shape class.
 * a) Add fields, create constructor, getters and setters.
 * b) Create classes Rectangle and Circle. Both of them should inherit class Shape.
 * Which fields and methods are common?
 */
public class Main {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[4];
        shapes[0] = new Rectangle("red", 6, 4);
        shapes[1] = new Rectangle("blue", 10, 8);
        shapes [2]=new Circle ("orange", 3);
        shapes [3]=new Circle("white", 8);

        Shape shapeWithMaxArea = shapes[0];
        for (Shape currentShape : shapes) {
            if (shapeWithMaxArea.getArea() < currentShape.getArea()) {
                shapeWithMaxArea = currentShape;
            }
        }
        System.out.println(shapeWithMaxArea);
    }
}




