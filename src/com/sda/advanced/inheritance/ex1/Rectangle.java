package com.sda.advanced.inheritance.ex1;

public class Rectangle extends Shape{
    private double length;
    private double width;

    public Rectangle(String color, double length, double width) { //am scos parametrii de care nu am nevoie si anume aria si perimetrul si la super am pus sa calculeze
        super(length*width,2*(length+width), color);
        this.length = length;
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return super.toString()+ "Rectangle{" +
                "length=" + length +
                ", width=" + width +
                '}';
    }
}
