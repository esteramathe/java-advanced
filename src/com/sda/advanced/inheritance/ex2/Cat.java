package com.sda.advanced.inheritance.ex2;

public class Cat extends Animal {
                       //daca nu are nimic specific(field) nu mai are nevoie de to string..
    public Cat(String color, String name) {
        super(color, name);
    }

    @Override
    public void yeldVoice() {
        System.out.println("MiauMiau!");
    }
}
