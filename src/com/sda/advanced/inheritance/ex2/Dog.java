package com.sda.advanced.inheritance.ex2;

public class Dog extends Animal {

    private double swimSpeed;

    public Dog(String color, String name, double swimSpeed) {
        super(color, name);
        this.swimSpeed = swimSpeed;
    }

    public void yeldVoice(){
        System.out.println("HamHam!");
    }

    public double getSwimSpeed() {
        return swimSpeed;
    }

    public void setSwimSpeed(double swimSpeed) {
        this.swimSpeed = swimSpeed;
    }

    @Override
    public String toString() {
        return super.toString() + "Dog{" +
                "swimSpeed=" + swimSpeed +
                '}';
    }
}
