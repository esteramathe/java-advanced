package com.sda.advanced.inheritance.ex2;

public class Animal {
    private String color;
    private String name;

    public Animal(String color, String name) {
        this.color = color;
        this.name = name;
    }

    public void yeldVoice() {
        System.out.println();
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "color='" + color + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
