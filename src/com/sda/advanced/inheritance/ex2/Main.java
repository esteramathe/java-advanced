package com.sda.advanced.inheritance.ex2;

import jdk.swing.interop.SwingInterOpUtils;

/**
 * Create classes Dog and Cat.
 * a) Move common methods and fields to the class Animal.
 * b) Create method „yieldVoice”.
 * c) Create simple array of type Animal, that will contain one object of type Dog and one object of type Cat.
 * d) Using for-each loop show which animal gives what kind of voice. How to print a name of an object?
 */
public class Main {
    public static void main(String[] args) {
        Animal animal1 = new Dog("brown", "Oscar", 30);
        animal1.yeldVoice();

        Animal[] animals = new Animal[2];

        animals[0]= new Dog("black", "Toto", 40);
        animals[1]=new Cat ("Brown", "Cici");

        for (Animal currentAnimal : animals) {
           currentAnimal.yeldVoice();
            System.out.println(currentAnimal.getClass().getName());//aici ne arata numele unui obiect/ne arata clasa (DOG si CAT)

           if(currentAnimal instanceof Dog){
                System.out.println("DOG found"+currentAnimal.getName());
                System.out.println("DOG swimSpeed"+((Dog) currentAnimal).getSwimSpeed());//daca am instanceof pe currentanimal (asta inseamna ca stiu sigur ca e de tip dog)putem face cast
            }
        }



    }
}
