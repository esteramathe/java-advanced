package com.sda.advanced.inheritance.ex3;

public class Main {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Jon", 1000, 2000, "10909");
        Employee employee2 = new Employee("Emy", 2000, 2010, "20909");
        Employee employee3 = new Employee("Jon", 2000, 2010, "10909");
        Person employee4 = new Employee("Jon", 1000, 2000, "10909");

        System.out.println(employee1.equals(employee4));
        System.out.println(employee2);
        System.out.println(employee3);
    }
}
