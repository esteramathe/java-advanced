package com.sda.advanced.inheritance.ex3;

import java.util.Objects;

public class Employee extends Person {
    private double annualSalary;
    private int startYear;
    private String nino;

    public Employee(String name, double annualSalary, int startYear, String nino) {
        super(name);
        this.annualSalary = annualSalary;
        this.startYear = startYear;
        this.nino = nino;
    }

    public double getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(double annualSalary) {
        this.annualSalary = annualSalary;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public String getNino() {
        return nino;
    }

    public void setNino(String nino) {
        this.nino = nino;
    }

    @Override
    public String toString() {
        return super.toString() + "Employee{" +
                "annualSalary=" + annualSalary +
                ", startYear=" + startYear +
                ", nino='" + nino + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Double.compare(employee.annualSalary, annualSalary) == 0 && startYear == employee.startYear && Objects.equals(nino, employee.nino);
    }

    @Override
    public int hashCode() {
        return Objects.hash(annualSalary, startYear, nino);
    }
}

