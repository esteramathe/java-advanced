package com.sda.advanced.lambda.ex1;

import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Lambda Exercise 2
 * Write a program that reads a collection of strings, separated by one or more whitespaces, from the console and then prints
 * them onto the console. Each string should be printed on a new line. Use a Consumer<T>.
 * - Input: Pesho Gosho Adasha
 * - Output:
 *   - Pesho
 *   - Gosho
 *   - Adasha
 */
public class Main {
    public static void main(String[] args) {
        String input="Pesho Gosho Adasha";
        Scanner scanner=new Scanner(System.in);
        process(input,word-> System.err.println("-" + word),()->scanner.nextLine());
    }
    private static void process(String input, Consumer<String> consumer, Supplier<String> supplier){
        for (String word : input.split(supplier.get())) {             // le splituieste si le pune pe fiecare linie
        consumer.accept(word);
        }
    }
}
