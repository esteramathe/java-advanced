package com.sda.advanced.interfaces.ex2;

public interface Graphic {

    String draw();
}
