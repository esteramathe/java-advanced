package com.sda.advanced.interfaces.ex2;

public class Square implements Graphic {
    @Override
    public String draw() {
        return "-------- | | | | --------";
    }
}
