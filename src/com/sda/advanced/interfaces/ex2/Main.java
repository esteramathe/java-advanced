package com.sda.advanced.interfaces.ex2;

/**
 * Create an interface 'Graphic' that encapsulates the fallowing method: - draw()
 * Based on this interface implement two classes: - Line: which should print at System out a line
 * ` "------------"`
 * - Square: which should print at System out a square:
 * ` -------- | | | | --------`
 * - Text: which should print at System out a text
 * Create a Main class where you should initialize an Array of type Graphic
 * with different implementations (Line, Square, Text).
 * Next, with a foreach call the draw() method on each of the array objects.
 */
public class Main {
    public static void main(String[] args) {
        Graphic[] graphic = new Graphic[3];
        graphic[0] = new Line();
        graphic[1] = new Square();
        graphic[2] = new Text();

        for (Graphic i : graphic) {
            System.out.println(i.draw());
        }
    }

}

