package com.sda.advanced.interfaces.ex2;

public class Text implements Graphic {

    @Override
    public String draw() {
        return "This is the text from Text class!";
    }
}
