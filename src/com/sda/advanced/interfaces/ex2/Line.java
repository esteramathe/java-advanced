package com.sda.advanced.interfaces.ex2;

public class Line implements Graphic {

    @Override
    public String draw() {
        return "------------";
    }

}
