package com.sda.advanced.interfaces.ex1;

import java.time.LocalDateTime;

public interface TimeSubscriber {

    void pushTimeAndDate(LocalDateTime localDateTime);
}
