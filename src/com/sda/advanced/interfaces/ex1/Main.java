package com.sda.advanced.interfaces.ex1;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        TimeSubscriber[] subscribers = new TimeSubscriber[10];
        for (int i = 0; i < subscribers.length / 2; i++) {
            subscribers[i] = new Client1();
            subscribers[subscribers.length - i - 1] = new Client2();
        }
        Server server = new Server(subscribers);
        server.start();

    }
}
