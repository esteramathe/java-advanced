package com.sda.advanced.interfaces.ex1;

import java.time.LocalDateTime;

public class Client2 implements  TimeSubscriber{

    @Override
    public void pushTimeAndDate(LocalDateTime localDateTime) {
        System.err.println(localDateTime);
    }
}
