package com.sda.advanced.interfaces.ex1;

import java.time.LocalDateTime;

public class Client1 implements TimeSubscriber{

    @Override
    public void pushTimeAndDate(LocalDateTime localDateTime) {
        System.out.println(localDateTime);
    }
}
