package com.sda.advanced.interfaces.ex1;

import java.time.LocalDateTime;

public class Server {
    private TimeSubscriber[] subscribers;

    public Server(TimeSubscriber[] subscribers) {
        this.subscribers = subscribers;
    }

    public void start() throws InterruptedException{
        for (int i = 0; i < 100000; i++) {
            for (TimeSubscriber subscriber : subscribers) {
                subscriber.pushTimeAndDate(LocalDateTime.now());
            }
           Thread.sleep(1000);
        }
    }
}
