package com.sda.advanced.abstractclass.ex2;

public class A extends Bank {

    private String balance = "$100";

    @Override
    public String getBalance() {
        return balance;
    }
}
