package com.sda.advanced.abstractclass.ex2;

public class B {

    private String balance = "$150";

    public String getBalance() {
        return balance;
    }
}
