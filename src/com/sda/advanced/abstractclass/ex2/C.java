package com.sda.advanced.abstractclass.ex2;

public class C {
    private String balance = "$200";

    public String getBalance() {
        return balance;
    }
}
