package com.sda.advanced.abstractclass.ex1;

public class Main {
    public static void main(String[] args) {
        Shape shape1 = new Rectangle(5, 10);
        Shape shape2 = new Rectangle(6, 12);

        Shape shape3 = new EquilateralTriangle(5, 10);
        Shape shape4                                    = new EquilateralTriangle(6, 12);

        int compareValue = shape1.compareTo(shape2);
        String sign;
        if (compareValue < 0) {
            sign = " < ";
        } else {
            sign = " = ";
        }

        System.out.println(shape1+ sign + shape2);
    }
}
