package com.sda.advanced.abstractclass.ex1;

public class EquilateralTriangle extends Triangle {

    public EquilateralTriangle(double base, double height) {
        super(base, height);
    }

    @Override
    public double getPerimeter() {
        return getBase()*3;  //get pt ca base e private
    }
}
