package com.sda.advanced.abstractclass.ex1;

public abstract class Shape {
    private int numOfSides;

    protected Shape(int numOfSides) {     //constructorul il pot face protected ca sa fie folosit doar de clasele care mostenesc clasa Shape
        this.numOfSides = numOfSides;
    }

    public int getNumOfSides() {
        return numOfSides;
    }

    public abstract double getArea();

    public abstract double getPerimeter();

    public int compareTo(Shape shape){
        return Double.compare(this.getArea(), shape.getArea()); //aici comparam aria Shapului meu cu aria shapului primit ca parametru prin metoda compare(metoda deja implementata pentru primitive)
    }

}
