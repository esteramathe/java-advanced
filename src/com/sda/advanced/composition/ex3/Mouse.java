package com.sda.advanced.composition.ex3;

public class Mouse {
    private int buttonCount;
    private boolean optical;

    public Mouse(int buttonCount, boolean optical){
        this.buttonCount=buttonCount;
        setButtonCount(buttonCount);
    }

    public int getButtonCount() {
        return buttonCount;
    }

    public void setButtonCount(int buttonCount) {
        this.buttonCount = buttonCount;
        buttonCount++;
    }

    public boolean isOptical() {
        return optical;
    }

    public void setOptical(boolean optical) {
        this.optical = optical;
    }

    @Override
    public String toString() {
        return "buttonCount= " + buttonCount +
                ", optical= " + optical ;
    }
}
