package com.sda.advanced.composition.ex3;

public class ComputerSet {
    private Mouse mouse;
    private Monitor monitor;

    public Mouse getMouse() {
        return mouse;
    }

    public void setMouse(Mouse mouse) {
        this.mouse = mouse;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public String toString() {
        return "ComputerSet{" +
                "mouse=" + mouse +
                ", monitor=" + monitor +
                '}';
    }
}
