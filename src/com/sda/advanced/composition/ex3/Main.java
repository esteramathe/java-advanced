package com.sda.advanced.composition.ex3;

public class Main {
    public static void main(String[] args) {
        Mouse mouse1 = new Mouse(1, true);
        System.out.println(mouse1);
        Monitor monitor1=new Monitor(92);
        System.out.println(monitor1);
    }
}
