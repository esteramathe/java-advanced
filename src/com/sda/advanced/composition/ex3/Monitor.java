package com.sda.advanced.composition.ex3;

public class Monitor {
    private int displaySize;

    public Monitor(int displaySize) {
        this.displaySize = displaySize;
        setDisplaySize(displaySize);
    }

    public int getDisplaySize() {
        return displaySize;
    }

    public void setDisplaySize(int displaySize) {
        if (displaySize > 0) {
            this.displaySize = displaySize;
        } else {
            System.err.println("Invalid size:" + displaySize);
        }
    }

    @Override
    public String toString() {
        return "displaySize= " + displaySize;
    }
}



